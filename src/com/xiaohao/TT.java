package com.xiaohao;

import java.io.IOException;

import fr.expression4j.basic.MathematicalElement;
import fr.expression4j.core.ConfigurationException;
import fr.expression4j.core.Expression;
import fr.expression4j.core.Parameters;
import fr.expression4j.core.exception.EvalException;
import fr.expression4j.core.exception.ParsingException;
import fr.expression4j.factory.ExpressionFactory;
import fr.expression4j.factory.NumberFactory;
/**
 * 嵌套函数的公式计算
 * 
 * @author xiaohao
 *
 */
public class TT {
	public static void main(String[] args)throws ParsingException,
	EvalException, ConfigurationException, IOException {
		
		String fun ="f(x)=x*2+5";
		
		String fun1 ="g(y,x)=f(x+1)*2+y*2";
		
		Expression expression1 = ExpressionFactory.createExpression(fun);
		Expression expression2 = ExpressionFactory.createExpression(fun1);
		
		MathematicalElement me1 = NumberFactory.createReal(1);
		MathematicalElement me2 = NumberFactory.createReal(2);
		
		Parameters parameters = ExpressionFactory.createParameters();
		
		parameters.addParameter("x", me1);
		
		System.out.println(fun+" x=1 : "+expression1.evaluate(parameters).getValue());
		
		parameters.addParameter("y", me2);
		
		System.out.println(fun1+" x=1 y=2 : "+expression2.evaluate(parameters).getValue());
		
		
		
		
	}
}
