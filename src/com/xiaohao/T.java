package com.xiaohao;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import fr.expression4j.basic.MathematicalElement;
import fr.expression4j.config.ConfigurationUtil;
import fr.expression4j.core.ConfigurationException;
import fr.expression4j.core.Expression;
import fr.expression4j.core.Parameters;
import fr.expression4j.core.exception.EvalException;
import fr.expression4j.core.exception.ParsingException;
import fr.expression4j.factory.ExpressionFactory;
import fr.expression4j.factory.NumberFactory;

/**
 * 简单公式的计算
 * 
 * @author xiaohao
 *
 */
public class T {

	public static void main(String[] args) throws ParsingException,
			EvalException, ConfigurationException, IOException {

		String fun = "f(aaa,baa,caa,daa)=aaa*10+baa*2+caa/daa";
		String fun1 = "f(a,b,c,d)=a*1+b*2+c/d";
		Expression expression1 = ExpressionFactory.createExpression(fun);
		Expression expression2 = ExpressionFactory.createExpression(fun);

		List<String> pList = expression1.getParameters();

		MathematicalElement me1 = NumberFactory.createReal(1.5);
		MathematicalElement me2 = NumberFactory.createReal(2);
		MathematicalElement me3 = NumberFactory.createReal(6);
		MathematicalElement me4 = NumberFactory.createReal(2);

		Parameters parameters = ExpressionFactory.createParameters();
		parameters.addParameter("aaa", me1);
		parameters.addParameter("baa", me2);
		parameters.addParameter("caa", me3);
		parameters.addParameter("daa", me4);

		System.out.println(expression1.evaluate(parameters).getRealValue());

		// store configuration in file
		OutputStream outStream = new FileOutputStream("d:/config.xml");
		ConfigurationUtil.saveConfig(outStream);
		outStream.close();

	}

}
