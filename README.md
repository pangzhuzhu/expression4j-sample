#expression4j-sample

expression4j 是一个公式计算器

**主要功能如下：**

1.  basic opératon (+,-,*,/,^)
2.  real value (3.4, 3e-3 ...)
3.  complex value (3.4+2e-3i)
4.  basic function like cos, sin as java Math class can do.
5.  complex function like f(x)=2*x+5  and g(x)=3*f(x+2)-x
6.  user define function writen in java
7.  user define gramar like g(x)=x#x (# is a custom operator) (this is managed by ExpressionModel)
8.  catalog (set of functions)
9.  XML configuration file (for storing/loading user functions and user define gramar)


这个项目用来进行简单的实验